<?php

require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use MagicBattle\{Game,
    Hero,
    MessagePrinter\ConsolePrinter,
    Game\Context,
    Game\Context\ChangesStack,
    Skill\RapidStrike,
    Skill\MagicShield
};


$gameContext = new Context(new ChangesStack());
$printer = new ConsolePrinter();
$game = new Game($gameContext, $printer, 20);
$orderus = new Hero($gameContext, 'Orderus', rand(70, 100), rand(70, 80),
    rand(45, 55), rand(40, 50), rand(10, 30), [new RapidStrike(), new MagicShield()]);
$beast = new Hero($gameContext, 'Beast', rand(60, 90), rand(60, 90), rand(40, 60),
    rand(40, 60), rand(25, 40));

$game->play([$beast, $orderus]);