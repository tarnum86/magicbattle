<?php
namespace MagicBattle\Helper;

/**
 * Class Luck
 * @package MagicBattle
 */
class Luck
{
    /**
     * @param int $possibility
     * @return bool
     */
    public static function isLuckyNow(int $possibility): bool
    {
        $randomNumber = rand(1, 100);
        return $possibility >= $randomNumber;
    }
}