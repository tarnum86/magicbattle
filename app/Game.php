<?php

namespace MagicBattle;

use MagicBattle\MessagePrinter\MessagePrinterInterface;
use MagicBattle\Game\Context;

/**
 * Class Game
 * @package MagicBattle
 */
class Game
{
    const DEFAULT_ROUNDS_NUMBER = 20;
    /**
     * @var MessagePrinterInterface
     */
    private MessagePrinterInterface $messagePrinter;
    /**
     * @var Context
     */
    private Context $gameContext;
    /**
     * @var int
     */
    private int $roundsNumber = self::DEFAULT_ROUNDS_NUMBER;

    /**
     * Game constructor.
     * @param Context $gameContext
     * @param MessagePrinterInterface $messagePrinter
     * @param int|null $roundsNumber
     */
    public function __construct(Context $gameContext, MessagePrinterInterface $messagePrinter,
                                int $roundsNumber = null)
    {
        $this->messagePrinter = $messagePrinter;
        $this->gameContext = $gameContext;
        if (!empty($roundsNumber)) {
            $this->roundsNumber = $roundsNumber;
        }
    }

    /**
     * @param array $heroes
     */
    public function play(array $heroes)
    {
        list($attacker, $defender) = $this->defineHeroesSequence($heroes);
        for ($increment = 0; $increment < $this->roundsNumber; $increment++) {
            $this->gameContext->setAttacker($attacker);
            $this->gameContext->setDefender($defender);
            $attacker->attack($defender);
            if (!$defender->isAlive()) {
                $this->gameContext->getChangesStack()->put(
                    'Battle is over! Winner is %s', $attacker->getName()
                );
                break;
            }
            // After the attack, the players switch roles: the attacker now defends and the defender now attacks
            list($attacker, $defender) = [$defender, $attacker];
        }
        $this->printGameResult();
    }

    /**
     * The application must output the results each turn: what happened,
     * which skills were used (if any),
     * the damage done, defender’s health left.
     */
    private function printGameResult()
    {
        $changes = $this->gameContext->getChanges();
        foreach ($changes as $change) {
            $this->messagePrinter->print($change);
        }
    }

    /**
     * The first attack is done by the player with the higher speed. If both players have the same speed,
     * then the attack is carried on by the player with the highest luck.
     *
     * Return array of heroes sorted by their sequence to attack
     *
     * @param array $heroes
     * @return array
     */
    public function defineHeroesSequence(array $heroes): array
    {
        usort($heroes, function ($hero1, $hero2) {
            if ($hero1->getSpeed() == $hero2->getSpeed()) {
                return ($hero1->getLuck() > $hero2->getLuck()) ? 1 : -1;
            }
            return ($hero1->getSpeed() > $hero2->getSpeed()) ? 1 : -1;
        });
        return $heroes;
    }
}