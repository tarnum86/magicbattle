<?php

namespace MagicBattle\Skill;

use MagicBattle\Game\Context;
use MagicBattle\Helper\Luck;
use MagicBattle\Observable;
use MagicBattle\Observer;

/**
 * Class SkillAbstract
 * @package MagicBattle
 */
abstract class SkillAbstract implements Observer
{
    /**
     * @const int Default possibility is 100%, each time
     */
    const DEFAULT_POSSIBILITY_PERCENTAGE = 100;

    /**
     * @return int
     */
    protected function getPossibility(): int
    {
        return self::DEFAULT_POSSIBILITY_PERCENTAGE;
    }

    /**
     * @param Context $gameContext
     * @return bool
     */
    public function canApply(Context $gameContext): bool
    {
        return Luck::isLuckyNow($this->getPossibility());
    }

    /**
     * @param Observable $subject
     * @return void
     */
    public function update(Observable $subject)
    {
        if ($this->canApply($subject)) {
            $this->apply($subject);
        }
    }

    /**
     * @param Context $gameContext
     * @return mixed
     */
    abstract public function apply(Context $gameContext);

    /**
     * @return string
     */
    abstract public function getEventToApply() : string;
}