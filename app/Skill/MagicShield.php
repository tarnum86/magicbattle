<?php

namespace MagicBattle\Skill;

use MagicBattle\Game\Context;

/**
 * Class MagicShield
 * @package MagicBattle\Skill
 */
class MagicShield extends SkillAbstract implements SkillInterface
{
    /**
     * @const int
     */
    const POSSIBILITY_PERCENTAGE = 20;
    /**
     * @const string
     */
    const EVENT_TO_APPLY = Context::EVENT_BEFORE_DEFENCE;

    /**
     * @return string
     */
    public function getEventToApply() : string
    {
        return self::EVENT_TO_APPLY;
    }
    /**
     * @return int
     */
    protected function getPossibility(): int
    {
        return self::POSSIBILITY_PERCENTAGE;
    }

    /**
     * Magic shield: Takes only half of the usual damage when an enemy attacks;
     *
     * @param Context $gameContext
     * @return mixed|void
     */
    public function apply(Context $gameContext)
    {
        $defender = $gameContext->getDefender();
        $attacker = $gameContext->getAttacker();

        $defenceIncreasing = ($attacker->getStrength() - $defender->getDefence()) / 2;
        $defender->setDefence($defender->getDefence() + $defenceIncreasing);
        $gameContext->getChangesStack()->put(
            'Magic Shield is used by %s.', $defender->getName()
        );
    }
}