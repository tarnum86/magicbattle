<?php
namespace MagicBattle\Skill;

use MagicBattle\Game\Context;

/**
 * Class RapidStrike
 * @package MagicBattleS\SkillAbstract
 */
class RapidStrike extends SkillAbstract implements SkillInterface
{
    /**
     * @const int
     */
    const POSSIBILITY_PERCENTAGE = 10;
    /**
     * @const string
     */
    const EVENT_TO_APPLY = Context::EVENT_AFTER_ATTACK;

    /**
     * @return string
     */
    public function getEventToApply() : string
    {
        return self::EVENT_TO_APPLY;
    }

    /**
     * @return int
     */
    protected function getPossibility(): int
    {
        return self::POSSIBILITY_PERCENTAGE;
    }

    /**
     * @param Context $gameContext
     * @return void
     * @throws \Exception
     */
    public function apply(Context $gameContext)
    {
        $attacker = $gameContext->getAttacker();
        $defender = $gameContext->getDefender();
        if ($defender->getHealth() <= 0) {
            return;
        }
        $gameContext->getChangesStack()->put(
            'Rapid Strike is used by %s.', $attacker->getName()
        );
        $attacker->attack($defender);
    }
}