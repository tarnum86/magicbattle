<?php
namespace MagicBattle\Skill;

use MagicBattle\Game\Context;

/**
 * Class SkillAbstract
 * @package MagicBattle
 */
interface SkillInterface
{
    /**
     * @param Context $gameContext
     * @return bool
     */
    public function canApply(Context $gameContext): bool;

    /**
     * @param Context $gameContext
     * @return void
     */
    public function apply(Context $gameContext);

    /**
     * @return string
     */
    public function getEventToApply() : string;
}