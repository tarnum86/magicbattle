<?php

namespace MagicBattle;

use MagicBattle\Game\Context;
use MagicBattle\Helper\Luck;
use MagicBattle\Skill\SkillInterface;

/**
 * Class Hero
 * @package MagicBattle
 */
class Hero
{
    /**
     * @var Context
     */
    private Context $gameContext;
    /**
     * @var string
     */
    private string $name;
    /**
     * @var int
     */
    private int $health;
    /**
     * @var int
     */
    private int $strength;
    /**
     * @var int
     */
    private int $defence;
    /**
     * @var int
     */
    private int $speed;
    /**
     * @var int
     */
    private int $luck;
    /**
     * @var array
     */
    private array $skills;


    /**
     * Hero constructor.
     * @param Context $gameContext
     * @param string $name
     * @param int $health
     * @param int $strength
     * @param int $defence
     * @param int $speed
     * @param int $luck
     * @param array $skills
     */
    public function __construct(Context $gameContext, string $name, int $health, int $strength,
                                int $defence, int $speed, int $luck, array $skills = [])
    {
        $this->gameContext = $gameContext;
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
        $this->defence = $defence;
        $this->speed = $speed;
        $this->luck = $luck;
        $this->skills = $skills;
        /** @var SkillInterface $skill */
        foreach ($skills as $skill) {
            $this->gameContext->attach($skill, $skill->getEventToApply());
        }
    }

    /**
     * The damage done by the attacker is calculated with the following formula:
     * Damage = Attacker strength – Defender defence
     * The damage is subtracted from the defender’s health. An attacker can miss their hit and do no
     * damage if the defender gets lucky that turn.
     *
     * @param Hero $defender
     * @throws \Exception
     */
    public function attack(Hero $defender)
    {
        $changesStack = $this->gameContext->getChangesStack();
        $changesStack->put(
            'Hero %s attacks %s.', $this->name, $defender->getName()
        );
        $this->gameContext->notify(Context::EVENT_BEFORE_ATTACK);

        $defender->defend($this);

        $this->gameContext->notify(Context::EVENT_BEFORE_ATTACK);
    }

    /**
     * @param Hero $attacker
     * @throws \Exception
     */
    public function defend(Hero $attacker)
    {
        $changesStack = $this->gameContext->getChangesStack();
        if (Luck::isLuckyNow($this->getLuck())) {
            $changesStack->put(
                'Hero %s misses hit because %s gets lucky!', $attacker->getName(), $this->name
            );
            return;
        }
        $this->gameContext->notify(Context::EVENT_BEFORE_DEFENCE);

        $damage = $attacker->getStrength() - $this->defence;
        $this->health -= $damage;
        $changesStack->put(
            'Damage done to hero %s is %d.', $this->name, $damage
        );
        $changesStack->put(
            'Defender health left is %d.', $this->health >= 0 ? $this->health : 0,
        );
        if ($this->health <= 0) {
            return;
        }
        $this->gameContext->notify(Context::EVENT_AFTER_DEFENCE);
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isAlive(): bool
    {
        return $this->health >= 0;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $defence
     */
    public function setDefence(int $defence)
    {
        $this->defence = $defence;
    }
}