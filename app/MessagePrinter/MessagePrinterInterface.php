<?php
namespace MagicBattle\MessagePrinter;

/**
 * Interface MessagePrinterInterface
 * @package MagicBattle\MessagePrinter
 */
interface MessagePrinterInterface
{
    /**
     * @param string $message
     * @return void
     */
    public function print(string $message);
}
