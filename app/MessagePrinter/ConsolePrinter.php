<?php
namespace MagicBattle\MessagePrinter;

/**
 * Class ConsolePrinter
 * @package MagicBattle\Message
 */
class ConsolePrinter implements MessagePrinterInterface
{
    /**
     * @param string $message
     */
    public function print(string $message)
    {
        echo $message . PHP_EOL;
    }
}