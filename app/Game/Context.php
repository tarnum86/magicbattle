<?php
namespace MagicBattle\Game;

use MagicBattle\Game\Context\ChangesStack;
use MagicBattle\Hero;
use MagicBattle\Observable;
use MagicBattle\Observer;

/**
 * Class Context
 * @package MagicBattle
 */
class Context implements Observable
{
    /**
     * @const string
     */
    const EVENT_BEFORE_ATTACK = 'event_before_attack';
    /**
     * @const string
     */
    const EVENT_AFTER_ATTACK = 'event_after_attack';
    /**
     * @const string
     */
    const EVENT_BEFORE_DEFENCE = 'event_before_defence';
    /**
     * @const string
     */
    const EVENT_AFTER_DEFENCE = 'event_after_defence';

    /**
     * @var ChangesStack
     */
    private ChangesStack $changesStack;

    /**
     * @var array
     */
    private array $observers = [];
    /**
     * @var Hero
     */
    private Hero $attacker;

    /**
     * @var Hero
     */
    private Hero $defender;

    /**
     * Context constructor.
     * @param ChangesStack $changesStack
     */
    public function __construct(ChangesStack $changesStack)
    {
        $this->changesStack = $changesStack;
    }

    /**
     * @param Observer $observer
     * @param string $eventName
     */
    public function attach(Observer $observer, string $eventName)
    {
        if (!isset($this->observers[$eventName])) {
            $this->observers[$eventName] = [];
        }
        $this->observers[$eventName][] = $observer;
    }

    /**
     * @param Observer $observer
     * @param string $eventName
     */
    public function detach(Observer $observer, string $eventName)
    {
        if (!empty($this->observers[$eventName]) && is_array($this->observers[$eventName])) {
            $key = array_search($observer, $this->observers[$eventName], true);
            if ($key) {
                unset($this->observers[$eventName][$key]);
            }
        }
    }

    /**
     * @param string $eventName
     */
    public function notify(string $eventName)
    {
        if (!empty($this->observers[$eventName]) && is_array($this->observers[$eventName])) {
            foreach ($this->observers[$eventName] as $observer) {
                $observer->update($this);
            }
        }
    }

    /**
     * @param Hero $attacker
     */
    public function setAttacker(Hero $attacker)
    {
        $this->attacker = $attacker;
    }

    /**
     * @return Hero
     */
    public function getAttacker(): Hero
    {
        return $this->attacker;
    }

    /**
     * @param Hero $defender
     */
    public function setDefender(Hero $defender)
    {
        $this->defender = $defender;
    }

    /**
     * @return Hero
     */
    public function getDefender(): Hero
    {
        return $this->defender;
    }

    /**
     * @return ChangesStack
     */
    public function getChangesStack(): ChangesStack
    {
        return $this->changesStack;
    }

    /**
     * @return array
     */
    public function getChanges(): array
    {
        return $this->changesStack->getChanges();
    }
}