<?php
namespace MagicBattle\Game\Context;

/**
 * Class ChangesStack
 * @package MagicBattle
 */
class ChangesStack
{
    /**
     * @var array
     */
    private array $changes = [];

    /**
     * @param string $change
     * @param mixed ...$variables
     */
    public function put(string $change, ...$variables)
    {
        $formattedChange = sprintf($change, ...$variables);
        $this->changes[] = $formattedChange;
    }

    /**
     * @return array
     */
    public function getChanges()
    {
        return $this->changes;
    }
}