<?php
namespace MagicBattle;

interface Observable {

    /**
     * @param Observer $observer
     * @param string $eventName
     * @return void
     */
    public function attach (Observer $observer, string $eventName);

    /**
     * @param Observer $observer
     * @param string $eventName
     * @return void
     */
    public function detach (Observer $observer, string $eventName);

    /**
     * @param string $eventName
     * @return void
     */
    public function notify (string $eventName);

}