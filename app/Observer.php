<?php
namespace MagicBattle;

interface Observer  {
    /**
     * @param Observable $subject
     * @return mixed
     */
    public function update (Observable $subject);
}