<?php

namespace MagicBattle\Test;

use MagicBattle\Game;
use MagicBattle\Game\Context;
use MagicBattle\MessagePrinter\ConsolePrinter;
use MagicBattle\Hero;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    /**
     * @var Game 
     */
    private Game $testedClassInstance;

    public function setUp(): void
    {
        $context = $this->createMock(Context::class);
        $printer = $this->createMock(ConsolePrinter::class);
        $this->testedClassInstance = new Game($context, $printer);
    }

    /**
     * @covers Game::defineHeroesSequence
     */
    public function testDefineFirstHeroWhenSpeedIsDifferent()
    {
        $hero1 = $this->createMock(Hero::class);
        $hero1->expects($this->any())
            ->method('getSpeed')
            ->will($this->returnValue(20));
        $hero2 = $this->createMock(Hero::class);
        $hero2->expects($this->any())
            ->method('getSpeed')
            ->will($this->returnValue(30));
        $this->assertEquals([$hero2, $hero1], $this->testedClassInstance->defineHeroesSequence([$hero1, $hero2]));
    }

    /**
     * @covers Game::defineHeroesSequence
     */
    public function testDefineFirstHeroWhenSpeedIsEqualButLuckIsDifferent()
    {
        $hero1 = $this->createMock(Hero::class);
        $hero1->expects($this->any())
            ->method('getSpeed')
            ->will($this->returnValue(20));
        $hero1->expects($this->any())
            ->method('getLuck')
            ->will($this->returnValue(20));
        $hero2 = $this->createMock(Hero::class);
        $hero2->expects($this->any())
            ->method('getLuck')
            ->will($this->returnValue(20));
        $hero1->expects($this->any())
            ->method('getLuck')
            ->will($this->returnValue(30));
        $this->assertEquals([$hero2, $hero1], $this->testedClassInstance->defineHeroesSequence([$hero1, $hero2]));
    }
}
