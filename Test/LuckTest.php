<?php
namespace MagicBattle\Test;

use MagicBattle\Helper\Luck;
use PHPUnit\Framework\TestCase;

class LuckTest extends TestCase
{
    public function testIsLuckyNowReturnTrueIsPossibilityIs100()
    {
        $this->assertTrue(Luck::isLuckyNow(100));
    }

    public function testIsLuckyNowReturnFalseIsPossibilityIs0()
    {
        $this->assertFalse(Luck::isLuckyNow(0));
    }
}
