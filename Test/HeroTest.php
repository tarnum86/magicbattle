<?php
namespace MagicBattle\Test;

use MagicBattle\Game\Context;
use MagicBattle\Hero;
use PHPUnit\Framework\TestCase;

class HeroTest extends TestCase
{
    /**
     * @covers Hero::attack
     * @throws \Exception
     */
    public function testAttackMakesDefenderHealthLess()
    {
        $context = $this->createMock(Context::class);
        $orderus = new Hero($context, 'Orderus', 70, 100, 70, 80, 55);
        $beast = new Hero($context, 'Beast', 70, 100, 70, 80, 0);
        $orderus->attack($beast);
        $this->assertEquals(40, $beast->getHealth());
    }
}
