<?php

namespace MagicBattle\Test;

use PHPUnit\Framework\TestCase;
use MagicBattle\Game\Context\ChangesStack;

class ChangesStackTest extends TestCase
{
    /**
     * @covers ChangesStack::put
     */
    public function testPutSimpleString()
    {
        $testedClassInstance = new ChangesStack();
        $testedClassInstance->put('abc');
        $this->assertEquals(['abc'], $testedClassInstance->getChanges());
    }

    /**
     * @covers ChangesStack::put
     */
    public function testPutFormattedString()
    {
        $testedClassInstance = new ChangesStack();
        $testedClassInstance->put('abc %s %d', 'string', 123);
        $this->assertEquals(['abc string 123'], $testedClassInstance->getChanges());
    }
}
