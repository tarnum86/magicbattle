<?php
use MagicBattle\Skill\MagicShield;
use MagicBattle\Game\Context;
use MagicBattle\Hero;
use PHPUnit\Framework\TestCase;

class MagicShieldTest extends TestCase
{

    /**
     * @covers MagicShield::apply
     */
    public function testApplyMakesDefenceIncreased()
    {
        $testedClassInstance = new MagicShield();
        $gameContext = $this->createMock(Context::class);
        $attacker = $this->createMock(Hero::class);
        $defender = new Hero($gameContext, 'Beast', 70, 100, 50, 80, 55);
        $gameContext->expects($this->once())
            ->method('getAttacker')
            ->will($this->returnValue($attacker));
        $gameContext->expects($this->once())
            ->method('getDefender')
            ->will($this->returnValue($defender));

        $attacker->expects($this->once())
            ->method('getStrength')
            ->will($this->returnValue(100));

        $testedClassInstance->apply($gameContext);
        $this->assertEquals(75, $defender->getDefence());
    }
}
